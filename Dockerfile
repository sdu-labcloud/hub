# base image
FROM node:10-alpine

# working directory
WORKDIR /usr/src/app

# copy dependency lists
COPY package.json package-lock.json ./

# install dependencies
RUN npm install --production

# copy source files
COPY . ./

# configure static build variables
ARG TAG

# configure dynamic environment variables
ENV PORT=80
ENV API_URL=""
ENV WEBAPP_URL=""
ENV TAG=${TAG}

# make port accessible
EXPOSE $PORT

# start application
CMD [ "node", "src/index" ]
