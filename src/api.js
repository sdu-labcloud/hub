const axios = require('axios');
const { get } = require('lodash');
const { config } = require('dotenv');
const logger = require('./logger');

config();

const apiUrl = process.env.API_URL;
if (!apiUrl) {
  logger.error('Missing environment variable: API_URL');
  process.exit(1);
}

exports.readMe = async (token) => {
  const axiosConfig = {
    headers: {
      Authorization: token,
    },
  };

  let response = null;

  try {
    response = await axios.get(`${apiUrl}/v1/me`, axiosConfig);
  } catch (err) {
    if (get(err, 'response.status', 0) === 404) {
      logger.warn('Unauthorized connection attempt.');
    } else {
      logger.error(err);
    }

    return null;
  }
  const identity = get(response, 'data.data', null);
  const model = get(response, 'data.model', null);

  if (!identity || !model) {
    return null;
  }

  return {
    ...identity,
    model,
  };
};
