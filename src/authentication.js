const api = require('./api');
const cache = require('./cache');
const logger = require('./logger');

module.exports = (handlers) => async (socket) => {
  const token = socket.handshake.headers.authorization;

  if (token) {
    const identity = await api.readMe(token);
    if (identity) {
      // Dot delimited keys for key-value storage.
      const socketKey = `sockets.${socket.id}`;
      const modelKey = `${identity.model}s.${identity.id}`;
      cache.set(socketKey, {
        model: identity.model,
        id: identity.id,
      });
      cache.set(modelKey, {
        ...identity,
        socketId: socket.id,
      });

      // Slash delimited room for REST-ish lingo.
      socket.join(`/${identity.model}s`);
      logger.info(`Socket connected: ${modelKey} via ${socketKey}`);

      // Clean cache on disconnect.
      socket.on('disconnect', () => {
        cache.reset(socketKey);
        cache.reset(modelKey);
        logger.info(`Socket disconnected: ${modelKey} via ${socketKey}`);
      });

      // Run authenticated handlers after authentication succeeded.
      return handlers.forEach((handler) => handler(socket));
    }

    // Disconnect if credentials are incorrect.
    return socket.disconnect(true);
  }

  // Disconnect if credentials are missing.
  return socket.disconnect(true);
};
