const cache = require('./cache');
const logger = require('./logger');

const capabilityEventPath = `/nodes/capabilities/raspberry_pi_camera`;

module.exports = (socket) => {
  // Get link between socket ID and entity to find model type and ID.
  const link = cache.get(`sockets.${socket.id}`);

  // Connected socket is a user.
  if (link.model === 'user') {
    // Send mime information on mime request.
    const onMime = (nodeId) => {
      const stream = cache.get(`streams.${nodeId}`);
      if (stream && stream.mime) {
        socket.emit(`${capabilityEventPath}/mime`, stream.mime);
      } else {
        // Retry to send mime information to client.
        setTimeout(() => {
          onMime(nodeId);
        }, 1000);
      }
    };
    socket.on(`${capabilityEventPath}/mime`, onMime);

    // Send initialization segment on init request.
    const onInit = (nodeId) => {
      const stream = cache.get(`streams.${nodeId}`);
      if (stream && stream.initialization) {
        socket.emit(
          `${capabilityEventPath}/initialization`,
          stream.initialization
        );
      } else {
        // Retry to send initialization segment to client.
        setTimeout(() => {
          onInit(nodeId);
        }, 1000);
      }
    };
    socket.on(`${capabilityEventPath}/initialization`, onInit);

    // Start stream or subscribe to existing stream.
    socket.on(`${capabilityEventPath}/start`, (nodeId) => {
      // Join room to receive video events.
      socket.join(`/nodes/${nodeId}/capabilities/raspberry_pi_camera`);

      // Stop video if current client is last client.
      const onStop = () => {
        const stream = cache.get(`streams.${nodeId}`);
        if (stream && stream.clientCount > 1) {
          const not = (reference) => (item) => item !== reference;
          const { clients } = stream;
          cache.set(`streams.${nodeId}.clients`, clients.filter(not(link.id)));
          cache.set(`streams.${nodeId}.clientCount`, stream.clientCount + 1);
        } else {
          cache.reset(`streams.${nodeId}`);
          const node = cache.get(`nodes.${nodeId}`);
          if (node) {
            socket.to(`${node.socketId}`).emit(`${capabilityEventPath}/stop`);
            logger.info(`Stopping video stream: nodes.${nodeId}`);
          }
        }
        socket.leave(`/nodes/${nodeId}/capabilities/raspberry_pi_camera`);
      };
      socket.on(`${capabilityEventPath}/stop`, onStop);
      socket.on('disconnect', onStop);

      // Update subscriber count or create stream if it does not exist.
      const stream = cache.get(`streams.${nodeId}`);
      if (stream) {
        cache.set(`streams.${nodeId}.clients`, [...stream.clients, link.id]);
        cache.set(`streams.${nodeId}.clientCount`, stream.clientCount + 1);
      } else {
        cache.set(`streams.${nodeId}.clients`, [link.id]);
        cache.set(`streams.${nodeId}.clientCount`, 1);
        const node = cache.get(`nodes.${nodeId}`);
        if (node) {
          socket.to(`${node.socketId}`).emit(`${capabilityEventPath}/start`);
          logger.info(`Starting video stream: nodes.${nodeId}`);
        }
      }
    });
  }

  // Connected socket is a node.
  if (link.model === 'node') {
    const streamKey = `streams.${link.id}`;

    // Forward "mime" events from camera.
    socket.on(`${capabilityEventPath}/mime`, (mime) => {
      socket
        .to(`/nodes/${link.id}/capabilities/raspberry_pi_camera`)
        .emit(`${capabilityEventPath}/mime`, mime);
      cache.set(`${streamKey}.mime`, mime);
    });

    // Forward "initialization" events from camera.
    socket.on(`${capabilityEventPath}/initialization`, (buffer) => {
      socket
        .to(`/nodes/${link.id}/capabilities/raspberry_pi_camera`)
        .binary(true)
        .emit(`${capabilityEventPath}/initialization`, buffer);
      cache.set(`${streamKey}.initialization`, buffer);
    });

    // Forward "segment" events from camera.
    socket.on(`${capabilityEventPath}/segment`, (buffer) => {
      socket
        .to(`/nodes/${link.id}/capabilities/raspberry_pi_camera`)
        .binary(true)
        .emit(`${capabilityEventPath}/segment`, buffer);
    });

    // Check if node has any active stream subscribers.
    const stream = cache.get(streamKey);
    if (stream) {
      const subs = stream.clientCount;
      const nodeKey = `nodes.${link.id}`;

      // Start streaming if there are open subscriptions.
      socket.emit(`${capabilityEventPath}/start`);
      logger.info(`Resuming video stream: ${nodeKey} to ${subs} clients`);

      // Forward stop event to clients.
      socket.on('disconnect', () => {
        socket
          .to(`/nodes/${link.id}/capabilities/raspberry_pi_camera`)
          .emit(`${capabilityEventPath}/stop`);
      });
    }
  }
};
