const { get, set } = require('lodash');

const cache = {};

exports.set = (key, value) => {
  set(cache, key, value);
};

exports.get = (key) => {
  return get(cache, key, null);
};

exports.reset = (key) => {
  const value = get(cache, key, undefined);
  set(cache, key, undefined);
  return value;
};
