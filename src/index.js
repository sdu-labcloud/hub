const http = require('http');
const socketio = require('socket.io');
const dotenv = require('dotenv');
const logger = require('./logger');
const authentication = require('./authentication');
const videostreams = require('./videostreams');

dotenv.config();

const port = process.env.PORT || 80;

const server = http.Server();
const io = socketio(server, {
  // Ensure that every website can connect to the server.
  handlePreflightRequest: (req, res) => {
    const { referer } = req.headers;
    const origin = referer ? new URL(referer).origin : '*';
    res.writeHead(200, {
      'Access-Control-Allow-Headers': 'Authorization',
      'Access-Control-Allow-Methods': '*',
      'Access-Control-Allow-Origin': origin,
      'Access-Control-Allow-Credentials': true,
    });
    res.end();
  },
});

io.on('connection', authentication([videostreams]));

server.listen(port, () => {
  logger.info(`Server started at: 0.0.0.0:${port}`);
});
