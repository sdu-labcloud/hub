# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.3] - 2020-09-10

### Fixed

- CI pipeline uses new interface to Docker daemon via TLS

## [0.2.2] - 2019-07-28

### Fixed

- Enable authentication permanently

## [0.2.1] - 2019-06-27

### Changed

- Temporarily remove authentication for demo

## [0.2.0] - 2019-06-25

### Fixed

- Implement proper authentication via webhook to REST API

## [0.1.0] - 2019-05-20

### Added

- [socket.io](https://socket.io) websocket server with fallback to long JSON polling
- Logging via [winston](https://github.com/winstonjs/winston)
- Environment variables via `.env`
- Code formatting with [Prettier](https://prettier.io)
- Linting with [ESLint](https://eslint.org/)
- [MIT License](https://mit-license.org/)
- CI with `.gitlab-ci.yml`
- Automatic tagging via [CHANGELOG.md](./CHANGELOG.md)
