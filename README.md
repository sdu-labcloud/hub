# LabCloud - Hub

A websocket server to distribute data and events of the "LabCloud" manufacturing platform.

## Usage

First create a `.env` file to store the configuration of the application.

```ini
# REST API server URL.
API_URL=https://api.thecore.sdu.dk
# Webpage URL.
WEBAPP_URL=https://thecore.sdu.dk
```

Make sure to have the latest LTS of [nodejs](https://nodejs.org/en/) installed. Afterwards run:

```shell
# Install the dependencies.
$ npm ci
# Start the application in development mode.
$ npm run dev
```
